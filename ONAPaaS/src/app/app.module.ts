import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { caseExperiencecomponent } from './components/caseExperience/caseExperience.component';
import { ProfileComponent } from './components/profile/profile.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NzTabsModule } from 'ng-zorro-antd';
import { NZ_I18N, en_US } from 'ng-zorro-antd';
import { ValidateService } from './services/validate.service';
//import { AuthService } from './services/auth.service';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthGuard } from './guards/auth.guard';
import { TenantComponent } from './components/tenant/tenant.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu'
import { MatToolbarModule } from '@angular/material/toolbar';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { LayoutComponent } from './components/_layout_paas/layout/layout.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LayoutAdminComponent } from './components/_layout_paas/layout-admin/layout-admin.component';
import { LayoutTenantComponent } from './components/_layout_paas/layout-tenant/layout-tenant.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ResourceCreationComponent } from './components/resource-creation/resource-creation.component';
import { UserTableComponent } from './components/user-table/user-table.component';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { ModelComponent } from './components/model/model.component';
import { SafeUrlSanitierPipe } from './pipe_f/safeUrlSanitier.pipe';
import { HelpComponent } from './components/help/help.component';
import { UsecasesComponent } from './components/usecases/usecases.component';
import { UsecasedetailsComponent } from './components/usecasedetails/usecasedetails.component';
import { MepComponent } from './components/mep/mep.component';
import { ChatComponent } from './chat/chat.component';
import { DialogflowService } from './services/dialogflow.service'
import { MessageListComponent, MessageFormComponent, MessageItemComponent } from './components';
import { MechomeComponent } from './components/mechome/mechome.component';
import { MecDataComponent } from './components/mec-data/mec-data.component';
import { HomedetailComponent } from './components/homedetail/homedetail.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    SafeUrlSanitierPipe,
    caseExperiencecomponent,
    ProfileComponent,
    TenantComponent,
    LayoutComponent,
    HomepageComponent,
    LayoutAdminComponent,
    LayoutTenantComponent,
    ResourceCreationComponent,
    UserTableComponent,
    AdminHomeComponent,
    ModelComponent,
    HelpComponent,
    UsecasesComponent,
    UsecasedetailsComponent,
    MepComponent,
    ChatComponent,
    // DialogflowService,
    MessageListComponent,
    MessageFormComponent,
    MessageItemComponent,
    MechomeComponent,
    MecDataComponent,
    HomedetailComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NzTabsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTableModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatToolbarModule,
    NoopAnimationsModule,
    MatGridListModule,
    MatIconModule,
    MatSelectModule,
    HttpClientModule,
    MatFormFieldModule,
    AppRoutingModule,
    FlashMessagesModule.forRoot(),
    ReactiveFormsModule,
    NgZorroAntdModule.forRoot(),
  ],
  exports: [

  ],
  providers: [ValidateService, AuthGuard,   DialogflowService, { provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
