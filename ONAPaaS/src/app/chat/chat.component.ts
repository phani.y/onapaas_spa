import { Component, OnInit, ElementRef } from '@angular/core';
import { Message } from 'app/components/model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  public message : Message;
  public messages : Message[];
  chat : boolean = false;
  chaticon : boolean = true;
  constructor(private _elementRef: ElementRef) { 

    this.message = new Message('', '../../../assets/user.png');
    this.messages = [
      new Message('Welcome to OnapPaas','../../../assets/user.png', new Date())
    ];
  }

  ngOnInit() {

  }
  openChatbox (){
    this.chaticon = false;
    this.chat = true;
  }
  closeChatbox(){
    this.chaticon = true;
    this.chat = false;
  }
}
