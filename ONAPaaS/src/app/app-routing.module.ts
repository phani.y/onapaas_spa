import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { caseExperiencecomponent } from './components/caseExperience/caseExperience.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuard } from './guards/auth.guard';
import { TenantComponent } from './components/tenant/tenant.component';
import { LayoutComponent } from './components/_layout_paas/layout/layout.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LayoutAdminComponent } from './components/_layout_paas/layout-admin/layout-admin.component';
import { LayoutTenantComponent } from './components/_layout_paas/layout-tenant/layout-tenant.component';
import { UserTableComponent } from './components/user-table/user-table.component';
import { ResourceCreationComponent } from './components/resource-creation/resource-creation.component';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { ModelComponent } from './components/model/model.component';
import { HelpComponent } from './components/help/help.component';
import { UsecasesComponent } from './components/usecases/usecases.component';
import { MepComponent } from './components/mep/mep.component';
import { ChatComponent } from './chat/chat.component';
import { MechomeComponent } from './components/mechome/mechome.component';
import { HomedetailComponent } from './components/homedetail/homedetail.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login-HOUP', component: LoginComponent },
  { path: 'help', component: HelpComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'paas', component: LayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: 'home', component: HomeComponent },
      { path: 'mepaas', component: MepComponent },
      { path: 'chat', component: ChatComponent },
      { path: 'homedetail', component: HomedetailComponent },
      { path: '**', redirectTo: '/paas/home', pathMatch: 'full' }]
  },
  { path: 'admin', component: LayoutAdminComponent,
    children: [
      { path: 'home', component: AdminHomeComponent },
      { path: 'mepaas', component: MepComponent },
      { path: 'homedetail', component: HomedetailComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'mec/home' , component:MechomeComponent,},
      { path: 'users', component: UserTableComponent },
      { path: 'resoucecreation', component: ResourceCreationComponent },
      { path: 'monitor', component: ModelComponent },
      { path: 'chat', component: ChatComponent },
      { path: 'dashboard', component: caseExperiencecomponent, canActivate: [AuthGuard] },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: '**', redirectTo: '/login-HOUP', pathMatch: 'full' }]
  },
  { path: 'tenant', component: LayoutTenantComponent,
    children: [
      { path: 'mepaas', component: MepComponent },
      { path: 'home', component: HomepageComponent, },
      { path: 'mec/home' , component:MechomeComponent,},
      { path: 'homedetail', component: HomedetailComponent },
      { path: 'tenant-dashboard', component: TenantComponent, },
      { path: 'usecases', component: UsecasesComponent, },
      { path: 'chat', component: ChatComponent },  
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
      { path: '**', redirectTo: '/login-HOUP', pathMatch: 'full' }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }