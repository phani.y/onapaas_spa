import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  @Input() state:any;
  menuClick: boolean = false;
  containerdata:boolean = true;
  showOVP: boolean = false;
  use: boolean = false;
  selectedState: string = "";
  homeData = [
    {
      "header": "OVP",
      "desciption": "OVP is a compatible test tool under LFN. It can be used to ensure that VNF/PNF has been fully tested before entering the ONAP system, and is compatible with ONAP systems and carriers' requirements for VNF/PNF, thereby simplifying VNF/PNF procurement.",
      "image": "../../../assets/home/ovp.png",
      "link": "http://www.ovpecosystem.com/#/"
    },
    { 
      "header": "Design & Distribution",
      "desciption": "The design state framework can help model reuse, and the efficiency can be further improved as the scale of available models increases. Resources, services, and products, as well as their management and control functions, can be modeled using a set of specifications and policies (such as rule sets) that control behavior and process execution.",
      "image": "../../../assets/home/dnd.png",
      "link":""
    },
    {
      "header": "Provisioning",
      "desciption": "Service provisioning is the process of setting up a service for a customer.This allows for the distribution of policy enforcement and templates among various ONAP modules such as the Servie Orchesterator(SO),"
      +" Controllers, Data Collection, Analytics and Events (DCAE), Active and Avaliable Inventroy (A&AI)"
      +", and a Security Framework. These conponents user common services that support loggin, access control, Multi-Site Sate Coordination (MUSIC), which allow the platform to register and manage state across Multi-Site deployment.",
      "image": "../../../assets/home/instantiation.png",
      "link":""
    },
    {
      "header": "Assurance",
      "desciption": "Service Assurance is the application of policies and processes to ensure that services offered over networks meet a pre-defined service quality level for and optimal subscriber experience.",
      "image": "../../../assets/a03c.png",
      "link":""
    },
    {
      "header": "Usecase",
      "desciption": "Provides a general ONAP sample to help users quickly understand the functions of each module of the ONAP and provide detailed operation steps to help users complete their first ONAP application in an end-to-end manner.",
      "image": "../../../assets/tenant-imges/a03e.png",
      "link":""
    },
  ]

  constructor(private router:Router) { }

  ngOnInit() {}

  ngOnChanges() {
    if(this.state != "" && this.state != undefined) {
      this.containerdata = true;
      this.menuClick = false;
    }
  }

  changeContent(e:any,lbl: string) {
    this.selectedState = lbl;
    this.menuClick = true;
    this.containerdata = false;
    if(this.selectedState == "Usecase" ){
      this.use = true;
      this.menuClick = false;
    }    
    if(lbl == 'OVP') {
      this.showOVP = true;
    }
  }

  closeIframe() {
    this.showOVP = false;
    this.containerdata = true;
  }
}
