import { Component, OnInit , Input} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mechome',
  templateUrl: './mechome.component.html',
  styleUrls: ['./mechome.component.css']
})
export class MechomeComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  @Input() state:any;
  menuClick: boolean = false;
  containerdata:boolean = true;
  showOVP: boolean = false;
  use: boolean = false;
  selectedState: string = "";
  selectedTab:string ='mec/home'
  homeData = [
    {
      "header": "MEC & MEPM",
      "desciption": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus nostrum blanditiis nesciunt id? Quidem fugiat quibusdam fuga deserunt corporis molestias! Dolores, voluptatibus omnis perferendis quas beatae odio voluptate quisquam sapiente?",
      "image": "../../../assets/home/ovp.png",
      "link": "http://www.ovpecosystem.com/#/"
    },
    { 
      "header": "ME APP",
      "desciption": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus nostrum blanditiis nesciunt id? Quidem fugiat quibusdam fuga deserunt corporis molestias! Dolores, voluptatibus omnis perferendis quas beatae odio voluptate quisquam sapiente?",
      "image": "../../../assets/home/dnd.png",
      "link":""
    },
    {
      "header": "MEC Developer",
      "desciption":"Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus nostrum blanditiis nesciunt id? Quidem fugiat quibusdam fuga deserunt corporis molestias! Dolores, voluptatibus omnis perferendis quas beatae odio voluptate quisquam sapiente?",
      "image": "../../../assets/home/instantiation.png",
      "link":""
    },
    {
      "header": "MEO Client",
      "desciption": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus nostrum blanditiis nesciunt id? Quidem fugiat quibusdam fuga deserunt corporis molestias! Dolores, voluptatibus omnis perferendis quas beatae odio voluptate quisquam sapiente?",
      "image": "../../../assets/a03c.png",
      "link":""
    }
  ]
  
  ngOnChanges() {
    this.selectedState = this.state;
    if(this.state != "" && this.state != undefined) {
      this.containerdata = false;
    }
  }

  changeContent(e:any,lbl: string) {
    this.selectedState = lbl;
    this.containerdata = false;
  }


}
