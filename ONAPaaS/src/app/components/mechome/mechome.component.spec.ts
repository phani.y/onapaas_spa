import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechomeComponent } from './mechome.component';

describe('MechomeComponent', () => {
  let component: MechomeComponent;
  let fixture: ComponentFixture<MechomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
