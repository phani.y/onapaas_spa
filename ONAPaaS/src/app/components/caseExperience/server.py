import os
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/my-link_instantiate/')
def my_link_i():
  print ('I got clicked under instantiate')
  #os.system('sh /root/oom/kubernetes/robot/ete-k8s.sh')
  os.system('sh /home/root1/Desktop/onapass/onapaas_spa/ONAPaaS/src/app/components/caseExperience/robo_i.sh')
  return 'Roboscript invoked sucessfully.'

@app.route('/my-link/')
def my_link():
  print ('I got clicked under design and distribute!')
  #os.system('sh /root/oom/kubernetes/robot/ete-k8s.sh')
  os.system('sh /home/root1/Desktop/onapass/onapaas_spa/ONAPaaS/src/app/components/caseExperience/robo.sh')
  return 'Roboscript design & distribute invoked sucessfully.'

@app.route('/my-link_healthcheck/')
def my_link_healthcheck():
  print ('I got clicked under health check!')
  os.system('sh /root/oom/kubernetes/robot/ete-k8s.sh')
  #os.system('sh /home/root1/Desktop/onapass/onapaas_spa/ONAPaaS/src/app/components/caseExperience/robo_healthcheck.sh')
  return 'Roboscript health check invoked sucessfully.'

if __name__ == '__main__':
  #app.run(debug=True)
  app.run( port=30997)

