import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { caseExperiencecomponent } from './caseExperience.component';

describe('caseExperiencecomponent', () => {
  let component: caseExperiencecomponent;
  let fixture: ComponentFixture<caseExperiencecomponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ caseExperiencecomponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(caseExperiencecomponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
