import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usecasedetails',
  templateUrl: './usecasedetails.component.html',
  styleUrls: ['./usecasedetails.component.css']
})
export class UsecasedetailsComponent implements OnInit {

  usecasecards: boolean = false;
  showUsecase:boolean = false;

  constructor() { }

  ngOnInit() {
    this.usecasecards = true;
  }
  openUsecase(){
    this.usecasecards = false;
    this.showUsecase = true;
  }
}
