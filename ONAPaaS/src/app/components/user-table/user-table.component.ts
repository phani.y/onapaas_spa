import { Component } from '@angular/core';
import {  Router } from '@angular/router';
import { Http } from '@angular/http';
import { Constants } from 'app/Contants';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent  {

  userData = {
    tenantId: 0,
    username: '',
    telephone :'',
    company: '',
    gender:'',
    address:'',
    permissions:[
      { role:'GUEST'},
      { role:'CUSTOMER'},
      { role:'CUSTOMER'},
      { role:'TENANT'}
    ]
  }

  onapSelect = [                                                                              
    {name:'GUEST',value:'GUEST'},                                                             
    {name:'TENANT',value:'TENANT'},                                                           
    {name:'ADMIN',value:'ADMIN'}                                                              
  ]                                                                                           
                                                                                              
  appStoreSelect = [                                                                          
    {name:'GUEST',value:'GUEST'},                                                             
    {name:'CUSTOMER',value:'CUSTOMER'},                                                       
    {name:'DEVELOPER',value:'DEVELOPER'},                                                     
    {name:'ADMIN',value:'ADMIN'},                                                             
    {name:'TENANT', value:'TENANT'}                                                           
  ] 
 

  meoSelect = [
    {name:'GUEST',value:'GUEST'},
    {name:'CUSTOMER',value:'CUSTOMER'},
    {name:'ADMIN',value:'ADMIN'}
  ] 
  mempSelect = [                                                               
    {name:'TENANT',value:'TENANT'},                                            
    {name:'ADMIN',value:'ADMIN'}                                                  
  ]                                                                               
                                                                                  
  listOfData = [];                                                                
                                                                                  
  display='none'; //default Variable                                              
  constructor(private router: Router, private http: Http) { }                     
                                                                                  
  ngOnInit(): void {                                                              
    sessionStorage.setItem('previousPath',this.router.url);                       
    this.getUsers();                                                              
  }                                                                               
                                                                                              
  getUsers():void {                                                                           
    let url = Constants.HTTP_URL +"houp/usr";                                                 
    this.http.get(url, {headers:Constants.HTTP_HEADERS}).subscribe((res:any)=>{               
      this.listOfData = JSON.parse(res._body);                                                
    });                                                                                       
  }                                                                                           
                                                                                              
  showModal(): void {                                                                         
    this.router.navigate(['admin/register']);                                                 
  }                                                                                           
                                                                                              
  modifyUser(user:any):void {  
    this.openModalDialog(); 
    let url = Constants.HTTP_URL +"houp/usr/"+user.tenantId;                   
    this.http.get(url, {headers:Constants.HTTP_HEADERS}).subscribe((res:any)=>{
      this.userData = JSON.parse(res._body);
    });                                                                                                                     
  }                                                                                           
                                               
  deleteUser(user:any):void {                
    let url = Constants.HTTP_URL +"houp/usr/"+user.tenantId;                   
    this.http.delete(url, {headers:Constants.HTTP_HEADERS}).subscribe((res:any)=>{
      if(res.status == "200") {                                                   
        this.getUsers();                                     
        alert("User delete successfully.");                 
      } else {                                                                    
        alert("User deletion failed.");                                        
      }                                       
    });                                                      
  }  
  
  saveUserData() {
    let url = Constants.HTTP_URL +"houp/usr/"+this.userData.tenantId;
    let tempObj = {};
    tempObj["username"] = this.userData.username;
    tempObj["tenantId"] = this.userData.tenantId;
    tempObj["company"] = this.userData.company;
    tempObj["telephone"] = this.userData.telephone;
    tempObj["gender"] = this.userData.gender;
    this.http.put(url,tempObj, {headers: Constants.HTTP_HEADERS}).subscribe((res:any)=>{
      //console.log(res._body);                                                               
      this.closeModalDialog();               
      this.getUsers();                                                          
    });  
  }
                                                             
  saveModalData() {                                                               
    let url = Constants.HTTP_URL +"houp/usr/permission/"+this.userData.tenantId;
    this.http.put(url,this.userData, {headers: Constants.HTTP_HEADERS}).subscribe((res:any)=>{
      //console.log(res._body);                                                               
      this.closeModalDialog();               
      this.getUsers();                                                          
    });                                                                                       
  }                                                                            
                                                                               
  openModalDialog(){                          
    this.display='block';                                    
  }                                                                             
                                                                                              
  closeModalDialog(){                                      
  this.display='none';                                                          
  }                                                                                           
}                                                            
    


 

  

