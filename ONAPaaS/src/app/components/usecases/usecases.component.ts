import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css']
})
export class UsecasesComponent implements OnInit {
  
  dMapEle:boolean = false;

  constructor( private _elementRef: ElementRef) { }

  resizeWindow() {
    this.dMapEle = false;
    if(window.innerWidth < 800) {
      this.dMapEle = true;
    }
  }

  ngOnInit() {
    this.resizeWindow();
    window.onresize = this.resizeWindow.bind(this);
  }
}
