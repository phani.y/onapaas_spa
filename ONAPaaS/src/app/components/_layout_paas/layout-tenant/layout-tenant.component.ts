import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TenantComponent } from '../../tenant/tenant.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-layout-tenant',
  templateUrl: './layout-tenant.component.html',
  styleUrls: ['./layout-tenant.component.css']
})
export class LayoutTenantComponent implements OnInit {
  menuitem = [
    {'name':"Design & Distribution"},
    {'name':"Provisioning"},
    {'name':"Assurance"},
    {'name':"Usecase", "submenuitem": [
      {'name':"vFirewallCL",'lblImg':'../../../../assets/tenant-imges/security.png',"link":""}]
    },
  ];

  mecMenuItem :any[] = [
    {'name':"MEC & MEPM"},
    {'name':"ME APP"},
    {'name':"MEC Developer"},
    {'name':"MEO Client"}
  ];

  homeClick: boolean = true;
  menuClick: boolean = false;
  deviceWidth:any = screen.width; 
  use: boolean = false;
  caseexperience: boolean = false;
  selectedState: string = "";
  isMobile:boolean = false;
  usecaseData : string = "Usecase";
  mepaasClick: boolean = false;
  mechomeClick:boolean = false;
  userType="tenant";
  routers:any;

  constructor(private router:Router, private _elementRef: ElementRef) {
    this.routers = router;
  }

  ngOnInit() {
    this.isMobile = this.deviceWidth < 575 ? true : false;
    sessionStorage.setItem('previousPath',this.router.url);
  }

  isMECItem(lbl:string) {
    let isMEC = false;
    for(let i = 0; i < this.mecMenuItem.length; i++) {
      if(this.mecMenuItem[i].name == lbl) {
        isMEC = true;
        break;
      }
    }
    return isMEC;
  }

  changeContent(e:any,lbl: string) {
    if(lbl == "Contact Us") {
      return;
    }
    sessionStorage.setItem("previousPath", this.routers.url + '/'+lbl);
    this.selectedState = "";
    this.homeClick = false;
    this.menuClick = false;
    this.use = false;
    this.caseexperience= false;
    this.mepaasClick = false;
    this.mechomeClick = false;
    if(lbl == "/tenant/home") {
      this.selectedState = lbl;
      this.homeClick = true;
    } else if(lbl == "/tenant/mec/home") {
      this.mechomeClick = true;
    } else if(lbl == 'Usecase') {
      this.use = true;
    } else if(lbl == 'vFirewallCL'){
      this.caseexperience = true;
    } else if(this.isMECItem(lbl)) {
      this.mechomeClick = true;
      this.selectedState = lbl;
    }
    else {
      this.selectedState = lbl;
      this.menuClick = true;
    }
    setTimeout(() => {
      this.selectedState = "";
    }, 100);
  }
}
