import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutTenantComponent } from './layout-tenant.component';

describe('LayoutTenantComponent', () => {
  let component: LayoutTenantComponent;
  let fixture: ComponentFixture<LayoutTenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutTenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutTenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
