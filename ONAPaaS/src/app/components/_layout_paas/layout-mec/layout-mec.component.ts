import { Component, OnInit , ContentChild} from '@angular/core';
import { Router } from '@angular/router';
import {MechomeComponent} from '../../../components/mechome/mechome.component'

@Component({
  selector: 'app-layout-mec',
  templateUrl: './layout-mec.component.html',
  styleUrls: ['./layout-mec.component.css']
})
export class LayoutMecComponent implements OnInit {

  routers:any

  mecmenuitem = [
    {'name':"MEC & MEPM"},
    {'name':"ME App"},
    {'name':"MEC client"},
    {'name':"MEC"},
  ];

  constructor(private router: Router) { 
    this.routers = router;
  }

 
  ngOnInit() {
    sessionStorage.setItem('previousPath',this.router.url);
  }

  changeContent(evt:any, lbl:string): void {
  
  }

}
