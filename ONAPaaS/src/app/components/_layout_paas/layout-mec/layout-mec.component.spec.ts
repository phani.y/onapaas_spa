import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutMecComponent } from './layout-mec.component';

describe('LayoutMecComponent', () => {
  let component: LayoutMecComponent;
  let fixture: ComponentFixture<LayoutMecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutMecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutMecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
