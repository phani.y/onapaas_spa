import { Component, OnInit, ElementRef,Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout-admin',
  templateUrl: './layout-admin.component.html',
  styleUrls: ['./layout-admin.component.css']
})

export class LayoutAdminComponent implements OnInit {
  userType="admin";
  menuitem = [
    {'name':"User Management",'routerLink':'admin/users'},
    {'name':"Resource Management",'routerLink':'admin/resoucecreation'},
    {'name':"Monitoring",'routerLink':'admin/monitor'},
    {'name':"Contact Us", "submenuitem": [
      {'name':"Slack",'lblImg':'../../../../assets/tenant-imges/slack.png',"link":""},
      //slack link https://app.slack.com/client/TNHR6QW3H/DN9H93L2V
      {'name':"Reach Us",'lblImg':'../../../../assets/tenant-imges/mail.png',"link":""}
    ]},
  ];
  selectedState = '';
  mechomeClick = false;
  mecMenuItem :any[] = [
    {'name':"MEC & MEPM"},
    {'name':"ME APP"},
    {'name':"MEC Developer"},
    {'name':"MEO Client"}
  ];

  constructor(private _elementRef: ElementRef, private router: Router) { }
  
  isMECItem(lbl:string) {
    let isMEC = false;
    for(let i = 0; i < this.mecMenuItem.length; i++) {
      if(this.mecMenuItem[i].name == lbl) {
        isMEC = true;
        break;
      }
    }
    return isMEC;
  }

  changeContent(e:any,lbl: string) {
    if(lbl == "Contact Us") {
      return;
    }
    sessionStorage.setItem("previousPath", this.router.url + '/'+lbl);
    this.selectedState = "";
    this.mechomeClick = false;
    if(this.isMECItem(lbl)) {
      this.mechomeClick = true;
      this.selectedState = lbl;
    }
   
    setTimeout(() => {
      this.selectedState = "";
    }, 100);
  }

  ngOnInit() {
    sessionStorage.setItem('previousPath',this.router.url);
  }
}
