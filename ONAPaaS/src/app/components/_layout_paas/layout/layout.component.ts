import { Component, OnInit, ElementRef, ContentChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
})

export class LayoutComponent implements OnInit {
  constructor(private _elementRef:ElementRef, private router: Router) { 
  }

  ngOnInit() {
    sessionStorage.setItem('previousPath',this.router.url);
  }
}
