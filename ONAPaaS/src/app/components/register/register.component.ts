import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
//import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { Constants } from 'app/Contants';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  userData:any = {};
  previousPath:string = '';

  constructor(
    private validateService: ValidateService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private http: Http) { }

  ngOnInit() {
    this.previousPath = sessionStorage.getItem("previousPath");
    sessionStorage.setItem("previousPath", this.router.url);
  }

  onCancel(): void {
    this.router.navigate([this.previousPath]);
  }

  isFormValid(): boolean {
    let isFormValid = true;
    if(Object.keys(this.userData).length == 0) {
      alert("Form cannot be emtpy");
      isFormValid = false;
    }  else if(!this.userData.username || this.userData.username == "") {
      isFormValid = false;
      alert("Username is empty.");
    } else if(!this.userData.password || !this.userData.confirmpassword || this.userData.password == "" || this.userData.confirmpassword == "") {
      isFormValid = false;
      alert("Password and Confirm password cannot be empty");
    } else if(this.userData.password != this.userData.confirmpassword) {
      isFormValid = false;
      alert("password doesn't match");
      this.userData.password = "";
      this.userData.confirmpassword = "";
    } else if(!this.userData.telephone || this.userData.telephone == "") {
      isFormValid = false;
      alert("Contact is empty.");
    }
    return isFormValid;
  }

  onRegisterSubmit() {
    if(!this.isFormValid()) {
      return;
    }
    
    let url = Constants.HTTP_URL +"houp/usr";
    this.http.post(url, this.userData).subscribe((res:any)=>{
      if(res.status == "201") {
        console.log(JSON.stringify(res._body));
        this.router.navigate([this.previousPath]);
      } else {
        alert("User register is falied.");
      }
      //console.log(res._body);
    });
  }
}
