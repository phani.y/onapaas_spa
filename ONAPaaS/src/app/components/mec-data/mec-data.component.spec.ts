import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MecDataComponent } from './mec-data.component';

describe('MecDataComponent', () => {
  let component: MecDataComponent;
  let fixture: ComponentFixture<MecDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MecDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MecDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
