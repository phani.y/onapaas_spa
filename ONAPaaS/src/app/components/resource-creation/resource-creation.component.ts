import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Constants } from 'app/Contants';

@Component({
  selector: 'app-resource-creation',
  templateUrl: './resource-creation.component.html',
  styleUrls: ['./resource-creation.component.css']
})

export class ResourceCreationComponent  {
  validateForm: FormGroup;
  title: any;
  showModel:any
  modelData:any;
  Models = ["Products", "Platform"]
  resourceList = ["All", "CDS","DG Builder","APPC CDT","UUI","CLAMP", "APP Store"];
  isResource = []
  originalIP:string;
  desc:any;
  url:any;
  resourceData:any;
  showData:boolean = true;

  constructor( private  http:Http,
    private route:ActivatedRoute,
    private router:Router)  { }

  private headers = new Headers({'Content-Type':'application/json'});

  getModelData(){
    let url = Constants.HTTP_URL +"houp/res";
    this.http.get(url, {headers:Constants.HTTP_HEADERS}).map(res => res.json()).subscribe(data => { 
      this.resourceData = data;
    });
  }



  getModel(model){
    if(model == "Products"){
      this.isResource = this.resourceList
    }
    else{
      this.isResource = []
      this.desc = ""
      this.url = "" 
    } 
  }

  udpateResource():void {
      this.showData = !this.showData; 
  }

  updateUrl(evt:any){
    let ipPattern = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    if(!this.url.match(ipPattern)) {
      alert("Please enter the valid IP address");
      return;
    }
    if(this.title == "All") {
      this.updateAllResource();
      return;
    }
    
    let str = this.modelData.url.replace(this.originalIP, this.url);

    let tempObj = {
      title:this.title,
      url:str,
      desc: this.desc
    }
    
    let url = Constants.HTTP_URL +"houp/res";
    this.http.put(url,tempObj, {headers: Constants.HTTP_HEADERS}).subscribe((res:any)=>{
      //console.log(res._body);
      this.showData = true;
      this.getModelData();
    });
    
  }

  updateAllResource() {
    let objData = [];
       
      for(let i = 0; i < this.resourceData.length; i++) {
        let urlIP = this.resourceData[i].url;
        if(urlIP) {
          let oldUrl = this.getIP(urlIP);
          urlIP = urlIP.replace(oldUrl, this.url);
          this.resourceData[i].url = urlIP
        }
      }
      
      let url = Constants.HTTP_URL +"houp/res";
      this.http.post(url, this.resourceData, {headers: Constants.HTTP_HEADERS }).map(res => res.json()).subscribe(data => {    
        this.showData = true;
        this.getModelData();
      });
  }

  getResource(resTitle:string) {
    if(resTitle == "All") {
      this.modelData = {};
      this.url = "";
      this.desc = "";
      return;
    }
    let url = Constants.HTTP_URL +"houp/res/"+ resTitle;
    this.http.get(url, {headers:Constants.HTTP_HEADERS}).map(res => res.json()).subscribe(data => { 
      this.modelData = data;
      this.desc = data.Desc;
      this.url = this.getIP(data.url);
      this.originalIP = this.url;
    });
    
  }

  getIP(url:string):string {
    var a = document.createElement('a')
    a.href = url;
    var ip = a.hostname;
    return ip;
  }

  ngOnInit(): void {
    sessionStorage.setItem('previousPath',this.router.url);
    this.Models
    this.getModelData();
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[ key ].markAsPristine();
      this.validateForm.controls[ key ].updateValueAndValidity();
    }
  }
}
