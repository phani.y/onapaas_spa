import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceCreationComponent } from './resource-creation.component';

describe('ResourceCreationComponent', () => {
  let component: ResourceCreationComponent;
  let fixture: ComponentFixture<ResourceCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
