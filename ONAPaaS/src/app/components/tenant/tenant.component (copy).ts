import { Component, OnInit,Input, OnChanges, ElementRef } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tenant',
  templateUrl: './tenant.component.html',
  styleUrls: ['./tenant.component.css']
})
export class TenantComponent implements OnInit, OnChanges {

  menuitem = ["DesignState", "Distribution", "Provisioning", "Assurance"];
  isShowIframe: boolean = false;
  cardData:any = {};
  @Input() state:any;

  // sdcUrl = "http://sdc.api.fe.simpledemo.onap.org:30206/sdc1/portal?cc=1566901125303";
  dataArr = {
      "Design & Distribution": {'theameimg':'../../../assets/tenant-imges/desing-distrubute.png', 'title':"Design & Distribute", 'contant':'The design state frame helps the model to be reused, along with the large size of the  available desert type. Can further improve efficiency. Resources, business and products, and itAnd the Lord! 0 can be modeled using a set of specifications and policies that control effort and process execution, such as Rule Sets.', 'data' : [{
      "id": 1,
      "title": "SDC",
      "desc": "SDC is the ONAP visual modeling and design tool. It creates internal metadata that describes assets used by all ONAP components, both at design time and run time.",
      "img" : "../../../assets/tenant-imges/sdc.png",
      "link": "../../../assets/testiframe.html?=sdc"
    },
    {
      "id": 2,
      "title": "DG Builder",
      "desc":"DG is Graphical UI which provides an execution environment for quickely write highly customized service flows",
      "img" : "../../../assets/tenant-imges/sdnc.png",
      "link": "",
    
    },
    {
      "id": 3,
      "title": "CDS",
      "desc":"CDS is a framework to automate the resolution of resources for instantiation and any config provisioning operation CDS has a both design time and run time activities; during design time, Designer can define what actions are required for a given service, along with anything comprising the action. The design produce a CBA Package . Its content is driven from a catalog of reusable data dictionary and component, delivering a reusable and simplified self service experience.",
      "img" : "../../../assets/tenant-imges/policy.png",
      "link": "",
    
    },
    {
      "id": 4,
      "title": "APPC CDT",
      "desc":"APPC Controller Design Tool (CDT) for self-service onboarding of VNF's. VNF owners can create templates and other arifacts for APPC Configure command (used to apply a post-instantiation configuration) as well as other life cycle coomands.",
      "img" : "../../../assets/tenant-imges/AAI.png",
      "link": "",
    }
    ]},
    "Provisioning": {'theameimg':'../../../assets/tenant-imges/instantiation.png',   'title':"Provisioning", 'contant':'Service provisioning is the process of setting up a service for a customer.'
    +'This allows for the distribution of policy enforcement and templates among various ONAP modules such as the Servie Orchesterator(SO), Controllers, Data Collection, Analytics and Events (DCAE), Active and Avaliable Inventroy (A&AI)'
    +', and a Security Framework. These conponents user common services that support loggin, access control, Multi-Site Sate Coordination (MUSIC), which allow the platform to register and manage state across Multi-Site deployment.  ', 'data': [
      {
        "id": 1,
        "title": "VID",
        "desc":"The Virtual Infrastructure Deployment (VID) application enables users to create or delete infrastructure services instances and their associated components, which have been designed in SDC.",
        "img" : "../../../assets/tenant-imges/policy.png",
        "link": "../../../assets/testiframe.html?=vid"
      },
      {
        "id": 2,
        "title": "UUI",
        "desc":"The Usecase User Interface (UUI) application enables users to perform LCM operation sepecific to a usecase. This application currently has the feasibility to perform, SOTN, CCVPN, SDWAN, VoLTE usecases",
        "img" : "../../../assets/tenant-imges/AAI.png",
        "link": ""
      }
    ]},
    "Assurance": {'theameimg':'../../../assets/tenant-imges/closeloop.png',  'title':"Assurance", 'contant':'Service Assurance is the application of policies and processes to ensure that services offered over networks meet a pre-defined service quality level for and optimal subscriber experience.', 'data': [
      {
        "id": 1,
        "title": "DCAE",
        "desc": "Data Collection Analytics and Events (DCAE) is the data collection and analysis subsystem of ONAP. Its tasks include collecting measurement, fault, status, configuration, and other types of data from network entities and infrastructure that ONAP interacts with, applying analytics on collected data, and generating intelligence (i.e. events) for other ONAP components such as Policy, APPC, and SDNC to operate upon; hence completing the ONAP’s close control loop for managing network services and applications.",
        "img" : "../../../assets/tenant-imges/sdc.png",
        "link": ""
      },
      {
        "id": 2,
        "title": "POLICY",
        "desc":"POLICY is a subsystem of ONAP that maintains, distributes, and operates on the set of rules that underlie ONAP’s control, orchestration, and management functions.", 
        "img" : "../../../assets/tenant-imges/policy.png",
        "link": ""
      },
      {
        "id": 3,
        "title": "CLAMP",
        "desc":"CLAMP is a platform for designing and managing control loops. It is used to design a Assurance, configure it with specific parameters for a particular network service, then deploying and undeploying it. Once deployed, the user can also update the loop with new parameters during runtime, as well as suspending and restarting it.",
        "img" : "../../../assets/tenant-imges/AAI.png",
        "link": ""
      }
    ]},
  };

  constructor(private http:Http, private _elementRef: ElementRef, private router:Router ) { }

  ngOnInit(){
    this.getIP();
  }

  getIP(){
    // this.http.get('http://localhost:5000/ONAPaaS_URLs').subscribe(data => {
    //   this.test(JSON.parse(data.text()));
    // });
    this.http.get('./config/url-config.txt').subscribe(data => {
       this.test(data.text());
    });
  }

  test(port:any){
    for(let dd in this.dataArr) {
      for(let i =0 ; i < this.dataArr[dd].data.length; i++){
        if(this.dataArr[dd].data[i].link == ""){
          // for(let k=0; k< port.length; k++) {
          //   if(this.dataArr[dd].data[i].title == port[k].Title) {
          //     this.dataArr[dd].data[i].link=port[k].url;
          //   }
          // }
          let url = this.getUrl(this.dataArr[dd].data[i].title, port);
          this.dataArr[dd].data[i].link = url;
          
        }
      }
    }
  }

  getUrl(title:string ,port:string) {
    let tempStr = port.split(title+'=')[1];
    let orgStr = tempStr ? tempStr.substring(0,tempStr.indexOf(',')) : '';
    return orgStr;
  }

  ngOnChanges() {
    if(this.state != "" && this.state != undefined) {
      this.cardData = this.dataArr[this.state];
      if(!this.cardData) {
        this.cardData = {};
      }
      this.closeFrame();
    }
  }

  showIframe(event: any) {
    if (event.currentTarget.pathname != "/") {
      this.isShowIframe = true;
    }
  }

  closeFrame() {
    this.isShowIframe = false;
  }
}
