import { Component, OnInit, ElementRef } from '@angular/core';
//import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { Constants } from 'app/Contants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  code:String = '';
  userData:any = {};
  captcha:String = '';
  isValidUsername: boolean = true;
  isValidCaptch: boolean = true;
  userType:string = 'paas';

  constructor(private router: Router,
    private http: Http) {}

  ngOnInit() {
    this.createCaptcha();
    sessionStorage.setItem("previousPath", this.router.url);
  }

  createCaptcha() {
    //clear the contents of captcha div first 
    var charsArray =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
      //below code will not allow Repetition of Characters
      var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
      if (captcha.indexOf(charsArray[index]) == -1)
        captcha.push(charsArray[index]);
      else i--;
    }
    this.code = captcha.join("");
  }

  onLoginSubmit() {
    this.isValidUsername = true;
    this.isValidCaptch = true;
    if (this.captcha.toLowerCase() == this.code.toLowerCase()) { 
      // if(this.username.toLowerCase()=="admin" && this.password=="admin") 
      // {
      //   this.userType = 'admin';
      //   this.router.navigate(['/admin/home']);
      // } else if((this.username.toLowerCase()=="operator" || this.username.toLowerCase()=="engineer")
      //   && this.password=="tenant")
      // {
      //   this.userType = 'tenant';
      //   this.router.navigate(['/tenant/home']);
        
      // } else {
      //   this.isValidUsername = false;
      //   this.password="";
      //   this.createCaptcha();
      //   alert("Incorrect username or password. Try Again");
      // }
      let url = Constants.HTTP_URL +"houp/auth";
      let dataObj = {
        username:this.userData.username,
        password:this.userData.password
      }
      

      this.http.post(url, JSON.stringify(dataObj)).subscribe((res:any)=>{
        if(res.status == "200") {
          let loginInfo = JSON.parse(res._body);
          document.cookie = "token=Basic"+loginInfo.token;
          url = Constants.HTTP_URL +"houp/usr/"+loginInfo.tenantId;
          
          let head = new Headers();
          res.headers.delete("content-length");
          
          head = res.headers;
          head.append("Authorization", "Basic "+loginInfo.token);

          this.http.get(url, {headers:Constants.HTTP_HEADERS}).subscribe((res:any)=>{
            let userInfo = JSON.parse(res._body);
            if(userInfo && userInfo.username == "admin") {
              this.userType = 'admin';
              this.router.navigate(['/admin/home']);
            }
            else if(userInfo && userInfo.username != "admin") {
              this.userType = 'tenant';
              this.router.navigate(['/tenant/home']);
            }
          });
        }
         else {
            this.isValidUsername = false;
            this.userData.password="";
            this.createCaptcha();
            alert("Incorrect username or password. Try Again");
          }
        
      });
    } else{
      this.isValidCaptch = false;
      alert("Invalid Captcha. Try Again");
      this.captcha="";
      this.createCaptcha();
    }
  }

  registerUser():void {
    this.router.navigate(['/register']);
  }
}
