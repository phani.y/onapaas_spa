import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosedloopComponent } from './closedloop.component';

describe('ClosedloopComponent', () => {
  let component: ClosedloopComponent;
  let fixture: ComponentFixture<ClosedloopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosedloopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosedloopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
