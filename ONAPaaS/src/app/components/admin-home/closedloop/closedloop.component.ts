import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-closedloop',
  templateUrl: './closedloop.component.html',
  styleUrls: ['./closedloop.component.css']
})
export class ClosedloopComponent implements OnInit {
  dataArr = [
    {
      "id": 1,
      "title": "DCAE",
      
      "link": "https://wiki.onap.org/pages/viewpage.action?pageId=1015831"
    },
    {
      "id": 2,
      "title": "POLICY",
      
      "link": "https://wiki.onap.org/display/DW/Policy"
    },

    {
      "id": 3,
      "title": "CLAMP",
      
      "link": "https://wiki.onap.org/pages/viewpage.action?pageId=4719898"
    }
  ]



  constructor(private router: Router) { }

  ngOnInit() {
    sessionStorage.setItem('previousPath',this.router.url);
  }

}
