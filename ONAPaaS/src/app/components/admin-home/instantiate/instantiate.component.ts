import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instantiate',
  templateUrl: './instantiate.component.html',
  styleUrls: ['./instantiate.component.css']
})
export class InstantiateComponent implements OnInit {
  dataArr = [
    {
      "id": 1,
      "title": "VID",
      
      "link": "https://wiki.onap.org/display/DW/VID"
    },
    {
      "id": 2,
      "title": "UUI",
     
      "link": "https://wiki.onap.org/display/DW/UUI+Component"
    }
   
    
  ]
  constructor() { }

  ngOnInit() {
  }

}
