import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  currentPageName:string;
  needLogout = true;
  constructor(private _elementRef: ElementRef, private router: Router) { }

  ngOnInit() {
    this.currentPageName = sessionStorage.getItem("previousPath");
    if(this.currentPageName) {
      if(this.currentPageName.indexOf('/login') > -1 || this.currentPageName.indexOf('/paas/home') > -1 
      || this.currentPageName.indexOf('/help') > -1) {
        this.needLogout = false;
      }
      sessionStorage.setItem('previousPath',this.router.url);
    }
  }
}
