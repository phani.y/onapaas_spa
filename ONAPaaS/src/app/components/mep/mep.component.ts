import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mep',
  templateUrl: './mep.component.html',
  styleUrls: ['./mep.component.css'],
})
export class MepComponent implements OnInit {

  selectedTab:string = 'mepaas';
  constructor() { }
  cardData = [
    {title:'Overview', desc:'overview'},
    {title:'Package List', desc:'Package List'},
    {title:'Package Distribution', desc:'Package Distribution'},
    {title:'Application LCM', desc:'Application LCM'},
    {title:'Instance List', desc:'Instance List'}
  ]
  ngOnInit() {
  }

  changeContent(evt: any, lbl:string):void {
    if(lbl.includes('mepaas')) {
      this.selectedTab = 'mepaas'
    } else {
      this.selectedTab = lbl;
    }
  }

}
