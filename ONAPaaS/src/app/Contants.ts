import {  Headers } from '@angular/http';

export class Constants {
    // public static get HTTP_URL(): string { return "http://159.138.53.90:30996/"; };
    //public static get HTTP_URL(): string { return "http://localhost:5000/"; };
    public static get HTTP_URL(): string { return "http://159.138.143.97:8080/v1/"; };

    public static get HTTP_HEADERS():any {
        
        function getCookie():string {
            let strCookie = document.cookie;
            let arrCookie = strCookie.split(";");
            for(let i = 0; i < arrCookie.length; i++) {
                let arr = arrCookie[i].split("=Basic");
                if(arr[0].trim() == "token") {
                    return "Basic "+arr[1];
                }
            }
            return "";
        }

        let cookieStr = getCookie();
        let header =  new Headers({
            //"Access-Control-Allow-Origin": "*",
            //"Access-Control-Allow-Methods": "GET,POST,DELETE,PUT",
            //"Access-Control-Allow-Headers": "X-Requested-With, Content-Type, X-Codingpedia,access-control-allow-credentials,access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,authorization",
            "Content-Type":'application/json',
            "Authorization": cookieStr
          });
         
        return header;
    }

    
}