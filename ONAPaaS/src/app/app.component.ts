import { Component, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from './Contants';
import { Http } from '@angular/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  menuItems:string[];
  changeContent:any;
  userType:string = 'paas';
  homeLink:string = '/paas/home';
  mePassLink:string = '/paas/mepaas';
  huaweiHomeLink:string = '/paas/homedetail';
  containerHeight:String = '0px';
  sideNavWidth:String = '0px';
  selectedTab:string = '';
  mecMenuItem :any[] = [
    {'name':"MEC & MEPM"},
    {'name':"ME APP"},
    {'name':"MEC Developer"},
    {'name':"MEO Client"}
  ];
  originalMenuItem = [];
  isHomeData = true;
  constructor(private _elementRef:ElementRef,
    private router: Router,
    private http : Http) {}

  ngOnInit() {
    this.setContentHeight();
  }

  changeTab(evt:any, lbl:string, tabName:string):void {
    this.selectedTab = '';
    if(tabName) {
      this.selectedTab = tabName;
    } 
    if(lbl.includes('/mec/home')) {
      this.menuItems = this.mecMenuItem;
      this.isHomeData = false;
    } else if(lbl.includes('/home')) {
      this.isHomeData = true;
      this.menuItems = this.originalMenuItem;
    }
      
    if(lbl != 'Contact Us') {
      this.changeContent(evt,lbl);
    }
  }

  changeGuestTab(evt:any,lbl:string): void {
    this.selectedTab = lbl;
  }

  changeMECTab(evt:any,lbl:string): void {
    if(lbl.includes('/mec/home')) {
      this.isHomeData = true;
      this.menuItems = this.mecMenuItem;
    } else  if(lbl.includes('/home')) {
      this.isHomeData = true;
      this.menuItems = this.originalMenuItem;
    }
    if(this.changeContent && this.isHomeData) {
      this.changeContent(evt,lbl);
    }
    this.selectedTab = lbl;
  }

  onActivate(componentReference:any) {
    this.menuItems = componentReference.menuitem;
    this.originalMenuItem = this.menuItems;
    if(componentReference.changeContent) {
      this.changeContent = componentReference.changeContent.bind(componentReference);
    }
    if(componentReference.userType && componentReference.userType == "paas") {
      this.homeLink = '/paas/home';
      this.mePassLink = '/paas/mepaas';
      this.huaweiHomeLink = '/paas/homedetail';
      this.userType = 'paas';
    }
    else if(componentReference.userType && componentReference.userType != "") {
      this.userType = componentReference.userType;
      this.homeLink = "/" + this.userType + '/home';
      this.mePassLink = "/" + this.userType + '/mec/home';
     
      this.huaweiHomeLink = "/" + this.userType + '/homedetail';
    } else {
      this.userType = 'paas';
    }
  }

  setContentHeight() {
    if(window.innerWidth > 800) {
      this.containerHeight = window.innerHeight - (65 + 35) + "px";
    } else {
      this.containerHeight = window.innerHeight - (45 + 35) + "px";
    } 
  }

  openNav(evt:any) {
    this.sideNavWidth = "230px";
    evt.stopPropagation();
  }

  closeNav(evt:any) {
    this.sideNavWidth = "0px";
    evt.stopPropagation();
  }

  logout() {
    this.selectedTab = '';
    let url = Constants.HTTP_URL + "houp/logout";
    this.http.post(url, {headers: Constants.HTTP_HEADERS }).subscribe((res:any)=>{
      console.log(res._body);
    });
  }
}
