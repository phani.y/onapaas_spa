const express = require('express');
const fs = require('file-system');
const bodyParser = require('body-parser');
var cors = require('cors');
const app = express();
let tokenId = '';
let userInfo = {
    id: 0,
    name: '',
    contact :'',
    company: '',
    gender:'',
    address:'',
    password:'',
    permission:{
      onapaas:'Guest',
      appstore:'Customer',
      meo:'Customer',
      meomemp:'None'
    }
  }
app.use(cors());
app.options('*', cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
  });

app.use(bodyParser.json());

app.get("/", (req, res) => {
    res.json({"msg":"server connected successfully."})
});

//get all user /houp/usr
app.get("/houp/usr", (req,res) => {
    fs.readFile('./src/config/UserInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        if(dataObj.ONAPaaS_Users) {
            res.send(dataObj.ONAPaaS_Users);
        }
    });
});

//get Specific user /houp/usr/{tenantID}
app.get("/houp/usr/:id", (req,res) => {
    console.log(JSON.stringify(req.headers));
    let tenantId = req.params.id;
    fs.readFile('./src/config/UserInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        let userData = dataObj.ONAPaaS_Users.filter(usr => {
            return tenantId == usr.id;
        }) 
        res.send(userData[0]);
    });
});

//register user using post request /houp/usr with user data
app.post("/houp/usr", (req,res) => {
    let obj = req.body;
    RegisterToLogin(obj);
    fs.readFile('./src/config/UserInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        let user = userInfo;
        user.id = getLastID(dataObj.ONAPaaS_Users)
        user.name = obj.name;
        user.contact = obj.contact;
        user.company = obj.company;
        user.address = obj.company;
        user.gender = obj.gender;
        user.type = "tenant";
        dataObj.ONAPaaS_Users.push(user);
        fs.writeFile('./src/config/UserInfo.json',JSON.stringify(dataObj));
        res.send(user);
    });
});

function RegisterToLogin (userData) {
    let obj = userData;
    fs.readFile('./src/config/UserLoginInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        let user = {};
        user.id = getLastID(dataObj.USER_LOGIN)
        user.name = obj.name;
        user.password = obj.password;
        user.type = "tenant";
        dataObj.USER_LOGIN.push(user);
        fs.writeFile('./src/config/UserLoginInfo.json',JSON.stringify(dataObj));
    });
}

//delete specific user using delete api  /houp/usr/{tenantID}
app.delete("/houp/usr/:id", (req,res) => {
    let tenantId = req.params.id;
    DeleteFromLogin(tenantId);
    fs.readFile('./src/config/UserInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        for(let i = 0; i < dataObj.ONAPaaS_Users.length; i++) {
            if(dataObj.ONAPaaS_Users[i].id == tenantId) {
                dataObj.ONAPaaS_Users.splice(i, 1);
            }
        }
        fs.writeFile('./src/config/UserInfo.json',JSON.stringify(dataObj));
        res.send('user deleted successfully');
    });
});

function DeleteFromLogin (userId) {
    let tenantId = userId;
    fs.readFile('./src/config/UserLoginInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        for(let i = 0; i < dataObj.USER_LOGIN.length; i++) {
            if(dataObj.USER_LOGIN[i].id == tenantId) {
                dataObj.USER_LOGIN.splice(i, 1);
            }
        }
        fs.writeFile('./src/config/UserLoginInfo.json',JSON.stringify(dataObj));
    });
}

//modify specific user using put api /houp/usr
app.put("/houp/usr", (req,res) => {
    let obj = req.body;
    fs.readFile('./src/config/UserInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        for(let i = 0; i < dataObj.ONAPaaS_Users.length; i++) {
            let tempUser = dataObj.ONAPaaS_Users[i];
            if(tempUser.id == obj.id) {
                dataObj.ONAPaaS_Users[i] = obj;
            }
        }
        fs.writeFile('./src/config/UserInfo.json',JSON.stringify(dataObj));
        res.send(obj);
    });
});

//login the user using post api with /houp/auth
app.post("/houp/auth", (req,res) => {
    let obj = JSON.stringify(req.body);
    fs.readFile('./src/config/UserLoginInfo.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        let loginUser = dataObj.USER_LOGIN.filter(user => {
            return (user.name.toLowerCase() == obj.username.toLowerCase() && 
            user.password == obj.password);
        })
        if(loginUser.length == 1) {
            tokenId = Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2)
            res.send({token:tokenId, tenantId:loginUser[0].id});
        }
        else {
            res.send({msg:"Failed"});
        }
    });  
});

app.get("/houp/res", (req, res) => {
    fs.readFile('./src/config/ONAPaaS.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        if(dataObj.ONAPaaS_URLs) {
            res.send(dataObj.ONAPaaS_URLs);
        }
    });
});

app.get("/houp/res/:title", (req, res) => {
    let title = req.params.title;
    fs.readFile('./src/config/ONAPaaS.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        if(dataObj.ONAPaaS_URLs) {
            let resData = dataObj.ONAPaaS_URLs.filter(resd => {
                return title == resd.Title;
            });
            if(resData.length > 0) {
                res.send(resData[0]);
            } else {
                res.send({});
            }
        }
    });
});

//modify specific resource using put api /houp/res
app.put("/houp/res", (req,res) => {
    let obj = req.body;
    fs.readFile('./src/config/ONAPaaS.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        if(dataObj.ONAPaaS_URLs) {
            for(let i = 0; i < dataObj.ONAPaaS_URLs.length; i++) {
                let tempData = dataObj.ONAPaaS_URLs[i];
                if(obj.title == tempData.Title) {
                    tempData.Desc = obj.desc;
                    tempData.url = obj.url;
                    dataObj.ONAPaaS_URLs[i] = tempData;
                    fs.writeFile('./src/config/ONAPaaS.json',JSON.stringify(dataObj));
                    break;
                }
            }
        }
        res.json({"msg":"Data successfully saved."});
    });
});

//modify all resource using put api /houp/res
app.post("/houp/res", (req,res) => {
    let obj = req.body;
    fs.readFile('./src/config/ONAPaaS.json', 'utf-8', function(err, data) {
        let dataObj = JSON.parse(data);
        if(dataObj.ONAPaaS_URLs) {
            dataObj.ONAPaaS_URLs = obj;
            fs.writeFile('./src/config/ONAPaaS.json',JSON.stringify(dataObj));
        }
        res.json({"msg":"Data successfully saved."});
    });
});

function getLastID(user) {
    let id = 1;
    for(let i = 0; i < user.length; i++) {
        if(user[i].id >= id) {
            id = user[i].id + 1;
        }
    }
    return id;
}

app.listen(process.env.PORT || 5000, () => {
    console.log("server is listening on port 5000");
});